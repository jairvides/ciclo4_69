const express=require('express');
const mgdb=require('../BaseDeDatos/conexion.js');
const enrutador=express.Router();
const nombreColeccion='Personas';
enrutador.use(express.json());
enrutador.use(express.urlencoded({extended:true}));
enrutador.route("/crear").post((req,res)=>{
	console.log(req.body);
	mgdb.model(nombreColeccion).insertMany([req.body],(error)=>{
		if(error)throw error;
		res.send({resultado:0});
	})
});
enrutador.route("/").get((req,res)=>{
	mgdb.model(nombreColeccion).find({},(error,datos)=>{
		if(error)throw error;
		res.send(datos);
	});
});
enrutador.route("/:id").get((req,res)=>{
	mgdb.model(nombreColeccion).find({id:req.params.id},(error,datos)=>{
		if(error)throw error;
		res.send(datos);
	});
});
enrutador.route("/actualizar").put((req,res)=>{
	mgdb.model(nombreColeccion).updateOne(req.body,(error)=>{
		if(error)throw error;
		res.send({resultado:0});
	});
});
enrutador.route("/eliminar").delete((req,res)=>{
	mgdb.model(nombreColeccion).deleteOne({id:req.body.id},(error)=>{
		if(error)throw error;
		res.send({resultado:0});
	});
});
module.exports=enrutador;